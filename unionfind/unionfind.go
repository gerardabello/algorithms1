package unionfind

//A UnionFind struct should be able to solve the union-find problem.
type UnionFind interface {

	//Initializes all the elements in the 'map'.
	//The number of elements is 'size'.
	Init(size int) error

	//Creates a connection between o1 and o2.
	Union(o1 int, o2 int) error

	//Checks if two elements o1 and o2 are connected.
	//Returns true if the elements are connected.
	Connected(o1 int, o2 int) (bool, error)
}
