package unionfind

import "errors"
import "strconv"
import "fmt"

//Implements the Quick Find algorithm
type UnionFind_QF struct {
        n       int     //size of the problem
        group   []int   //indicates in withc group the object 'i' is part of
}

func (uf *UnionFind_QF) Init(size int) error {

        if size <= 0 {
                return errors.New("Size must be greater than 0")
        }

        uf.n = size

        uf.group = make([]int, uf.n)

        for i := 0; i < uf.n; i++ {
                uf.group[i] = i
        }

        return nil
}

func (uf *UnionFind_QF) validateIndex(o int) error {

        if uf.n == 0 {
                return errors.New("Data not initializated")
        }

        if o >= uf.n || o < 0 {
                return errors.New("Index out of range " + strconv.Itoa(o))
        }

        return nil
}

func (uf *UnionFind_QF) Union(o1 int, o2 int) error {

        err := uf.validateIndex(o1)
        if err != nil {
                return err
        }

        err = uf.validateIndex(o2)
        if err != nil {
                return err
        }

        var from_group int = uf.group[o1]
        var to_group int = uf.group[o2]

        for i := range uf.group {
                if uf.group[i] == from_group {
                        uf.group[i] = to_group
                }
        }

        fmt.Printf("%n,%n \n", o1, o2)
        fmt.Printf("%v \n", uf.group)

        return nil
}

func (uf UnionFind_QF) Connected(o1 int, o2 int) (bool, error) {

        err := uf.validateIndex(o1)
        if err != nil {
                return false, err
        }

        err = uf.validateIndex(o2)
        if err != nil {
                return false, err
        }

        return uf.group[o1] == uf.group[o2], nil
}
