package unionfind

//Implements the Weightened Quick Union with path reduction
type UnionFind_QU_W_PR struct {
	UnionFind_QU_W
}

func (uf UnionFind_QU_W_PR) root(o int) (int, error) {

	err := uf.validateIndex(o)
	if err != nil {
		return 0, err
	}

	//this eventually compresses the length of virtually all trees to 1
	uf.parent[o] = uf.parent[uf.parent[o]]

	var parent int = uf.parent[o]
	if parent != o {
		return uf.root(parent)
	} else {
		return parent, nil
	}
}
