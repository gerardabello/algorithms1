package unionfind

import "errors"
import "fmt"

//Implements the Weightened Quick Union with tree lenght as weight
type UnionFind_QU_W struct {
        UnionFind_QU
        size    []int
}

func (uf *UnionFind_QU_W) Init(size int) error {

        if size <= 0 {
                return errors.New("Size must be greater than 0")
        }

        uf.n = size

        uf.parent = make([]int, uf.n)
        uf.size = make([]int, uf.n)

        for i := 0; i < uf.n; i++ {
                uf.parent[i] = i
                uf.size[i] = 1
        }

        return nil
}

func (uf *UnionFind_QU_W) Union(o1 int, o2 int) error {

        err := uf.validateIndex(o1)
        if err != nil {
                return err
        }

        err = uf.validateIndex(o2)
        if err != nil {
                return err
        }

        root1, _ := uf.root(o1)
        root2, _ := uf.root(o2)

        //Attach the shortest tree to the longest one to avoid increassing the lenght of the longer one
        if uf.size[root1] < uf.size[root2] {
                uf.parent[root1] = root2
        } else if uf.size[root1] > uf.size[root2] {
                uf.parent[root2] = root1
        } else {
                //if they are equal in lenght, one of them will have to increase
                uf.parent[root1] = root2
                uf.size[root2] += 1
        }

        fmt.Printf("%v", uf.parent)

        return nil
}
