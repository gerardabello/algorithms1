package unionfind

import "testing"
import "errors"
import "math/rand"
import "github.com/GerardAbello/algorithms1/testreader"
import "strconv"

func TestQF(t *testing.T) {

        var uf *UnionFind_QF = new(UnionFind_QF)

        err := testUF(uf)

        if err != nil {
                t.Log(err)
                t.Fail()
        }
}

func TestQU(t *testing.T) {

        var uf *UnionFind_QU = new(UnionFind_QU)

        err := testUF(uf)

        if err != nil {
                t.Log(err)
                t.Fail()
        }
}

func TestQU_W(t *testing.T) {

        //var uf *UnionFind_QU_W = &UnionFind_QU_W{UnionFind_QU : &UnionFind_QU{}}
        var uf *UnionFind_QU_W = new(UnionFind_QU_W)

        err := testUF(uf)

        if err != nil {
                t.Log(err)
                t.Fail()
        }
}

func TestQU_W_PR(t *testing.T) {

        var uf *UnionFind_QU_W_PR = new(UnionFind_QU_W_PR)

        err := testUF(uf)

        if err != nil {
                t.Log(err)
                t.Fail()
        }
}

func BenchmarkQuickFind(b *testing.B) {

        var uf *UnionFind_QF = new(UnionFind_QF)

        err := benchUF(uf, 2e7)

        if err != nil {
                b.Log(err)
                b.Fail()
        }
}

func BenchmarkQuickUnion(b *testing.B) {

        var uf *UnionFind_QU = new(UnionFind_QU)

        err := benchUF(uf, 2e7)

        if err != nil {
                b.Log(err)
                b.Fail()
        }
}

func BenchmarkQuickUnionWeight(b *testing.B) {

        var uf *UnionFind_QU_W = new(UnionFind_QU_W)

        err := benchUF(uf, 2e7)

        if err != nil {
                b.Log(err)
                b.Fail()
        }
}

func BenchmarkQuickUnionWeightPathReduction(b *testing.B) {

        var uf *UnionFind_QU_W_PR = new(UnionFind_QU_W_PR)

        err := benchUF(uf, 2e7)

        if err != nil {
                b.Log(err)
                b.Fail()
        }
}

//## Generic tests

func testUF(uf UnionFind) error {

        n, values, err := testreader.GetTestValuesInt("./union.csv")

        if err != nil {
                return err
        }

        uf.Init(n)

        for _, v := range values {
                err = uf.Union(v[0], v[1])

                if err != nil {
                        return err
                }
        }

        old_n := n

        n, values, err = testreader.GetTestValuesInt("./find.csv")

        if n != old_n {
                return errors.New("Union and find datasets do not have the same size")
        }

        var c bool

        for _, v := range values {
                c, err = uf.Connected(v[0], v[1])

                if err != nil {
                        return err
                }

                //if v[2] == 1 it means they should be connected
                expected := v[2] == 1

                if expected != c {
                        return errors.New("Incorrect find " + strconv.Itoa(v[0]) + ", " + strconv.Itoa(v[1]))
                }
        }

        return nil

}

func benchUF(uf UnionFind, n int) error {

        uf.Init(n)

        var r int = n / 2

        var err error

        for i := 0; i < r; i++ {
                err = uf.Union(rand.Intn(n-1), rand.Intn(n-1))

                if err != nil {
                        return err
                }
        }

        for i := 0; i < r; i++ {
                _, err = uf.Connected(rand.Intn(n-1), rand.Intn(n-1))

                if err != nil {
                        return err
                }
        }

        return nil

}
