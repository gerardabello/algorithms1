package unionfind

import "errors"
import "strconv"

//Implements the Quick Union algorithm
type UnionFind_QU struct {
	n      int
	parent []int
}

func (uf *UnionFind_QU) Init(size int) error {

	if size <= 0 {
		return errors.New("Size must be greater than 0")
	}

	uf.n = size

	uf.parent = make([]int, uf.n)

	for i := 0; i < uf.n; i++ {
		uf.parent[i] = i
	}

	return nil
}

func (uf *UnionFind_QU) validateIndex(o int) error {

	if uf.n == 0 {
		return errors.New("Data not initializated")
	}

	if o >= uf.n || o < 0 {
		return errors.New("Index out of range " + strconv.Itoa(o))
	}

	return nil
}

func (uf *UnionFind_QU) Union(o1 int, o2 int) error {

	err := uf.validateIndex(o1)
	if err != nil {
		return err
	}

	err = uf.validateIndex(o2)
	if err != nil {
		return err
	}

	root1, _ := uf.root(o1)
	root2, _ := uf.root(o2)

	uf.parent[root1] = root2

	return nil
}

func (uf UnionFind_QU) Connected(o1 int, o2 int) (bool, error) {

	err := uf.validateIndex(o1)
	if err != nil {
		return false, err
	}

	err = uf.validateIndex(o2)
	if err != nil {
		return false, err
	}

	root1, _ := uf.root(o1)
	root2, _ := uf.root(o2)

	return root1 == root2, nil
}

func (uf UnionFind_QU) root(o int) (int, error) {

	err := uf.validateIndex(o)
	if err != nil {
		return 0, err
	}

	var parent int = uf.parent[o]
	if parent != o {
		return uf.root(parent)
	} else {
		return parent, nil
	}
}
