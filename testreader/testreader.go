package testreader

import "encoding/csv"
import "os"
import "strconv"

func GetTestValues(file string) (string, [][]string, error) {
	csvfile, err := os.Open(file)

	if err != nil {
		return "", nil, err
	}

	defer csvfile.Close()

	reader := csv.NewReader(csvfile)

	reader.FieldsPerRecord = -1 // see the Reader struct information below

	rawCSVdata, err := reader.ReadAll()

	if err != nil {
		return "", nil, err
	}

	//first line only has one value and it's the size of the problem
	size := rawCSVdata[0][0]
	//exclude the first line
	values := rawCSVdata[1:]

	return size, values, nil
}

func GetTestValuesInt(file string) (int, [][]int, error) {

	size_s, values_s, err := GetTestValues(file)

	if err != nil {
		return 0, nil, err
	}

	values_sx := len(values_s)
	values_sy := len(values_s[0])

	values := make([][]int, values_sx)
	for i := range values {
		values[i] = make([]int, values_sy)
	}


	size, err_p := strconv.ParseInt(size_s, 10, 64)

	if err_p != nil {
		return 0, nil, err
	}

	for i, e := range values_s {

		for j, ei := range e {
			var v int64
			v, err_p = strconv.ParseInt(ei, 10, 64)

			if err_p != nil {
				return 0, nil, err
			}

			values[i][j] = int(v)
		}
	}

	return int(size), values, nil
}
